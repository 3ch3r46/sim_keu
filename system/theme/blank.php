
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Metro, a sleek, intuitive, and powerful framework for faster and easier web development for Windows Metro Style.">
    <meta name="keywords" content="HTML, CSS, JS, JavaScript, framework, metro, front-end, frontend, web development">
    <meta name="author" content="Sergey Pimenov and Metro UI CSS contributors">

    <link rel='shortcut icon' type='image/x-icon' href='../favicon.ico' />

    <title>Sistem Keuangan Teknik Informatika</title>

    <link href="<?= $_url ?>assets/css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= $_url ?>assets/css/flat-ui.css" rel="stylesheet">

    <script src="<?= $_url ?>assets/js/vendor/jquery.min.js"></script>
    <script src="<?= $_url ?>assets/js/flat-ui.min.js"></script>
 
</head>
<body>
    <?= $_content ?>
</body>
</html>