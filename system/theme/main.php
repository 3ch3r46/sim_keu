<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Metro, a sleek, intuitive, and powerful framework for faster and easier web development for Windows Metro Style.">
    <meta name="keywords" content="HTML, CSS, JS, JavaScript, framework, metro, front-end, frontend, web development">
    <meta name="author" content="Sergey Pimenov and Metro UI CSS contributors">

    <link rel="shortcut icon" type="image/png" href="<?= $_url ?>assets/img/logo.png">

    <title>Sistem Keuangan Teknik Informatika</title>
    <link href="<?= $_url ?>assets/css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= $_url ?>assets/css/flat-ui.css" rel="stylesheet">
    
    <script src="<?= $_url ?>assets/js/vendor/jquery.min.js"></script>
    <script src="<?= $_url ?>assets/js/flat-ui.min.js"></script>

    <style>
        /*
        * Base structure
        */

        /* Move down content because we have a fixed navbar that is 50px tall */
        body {
        padding-top: 50px;
        }


        /*
        * Global add-ons
        */

        .sub-header {
        padding-bottom: 10px;
        border-bottom: 1px solid #eee;
        }

        /*
        * Top navigation
        * Hide default border to remove 1px line.
        */
        .navbar-fixed-top {
        border: 0;
        }

        /*
        * Sidebar
        */

        /* Hide for mobile, show later */
        .sidebar {
        display: none;
        }
        @media (min-width: 768px) {
        .sidebar {
            position: fixed;
            top: 51px;
            bottom: 0;
            left: 0;
            z-index: 1000;
            display: block;
            padding: 20px;
            overflow-x: hidden;
            overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
            background-color: #f5f5f5;
            border-right: 1px solid #eee;
        }
        }

        /* Sidebar navigation */
        .nav-sidebar {
        margin-right: -21px; /* 20px padding + 1px border */
        margin-bottom: 20px;
        margin-left: -20px;
        }
        .nav-sidebar > li > a {
        padding-right: 20px;
        padding-left: 20px;
        }
        .nav-sidebar > .active > a,
        .nav-sidebar > .active > a:hover,
        .nav-sidebar > .active > a:focus {
        color: #fff;
        background-color: #428bca;
        }


        /*
        * Main content
        */

        .main {
        padding: 20px;
        }
        @media (min-width: 768px) {
        .main {
            padding-right: 40px;
            padding-left: 40px;
        }
        }
        .main .page-header {
        margin-top: 0;
        }


        /*
        * Placeholder dashboard ideas
        */

        .placeholders {
        margin-bottom: 30px;
        text-align: center;
        }
        .placeholders h4 {
        margin-bottom: 0;
        }
        .placeholder {
        margin-bottom: 20px;
        }
        .placeholder img {
        display: inline-block;
        border-radius: 50%;
        }

    </style>
</head>
<body>

    <?php
    $checkActive = function($folder) use ($_folder) {
      if ($folder == $_folder) {
        return 'class="active"';
      }
    };
    ?>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?= $_url ?>">Sistem Keuangan Teknik Informatika</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right hidden-lg hidden-md hidden-sm">
            <li <?= $checkActive('dashboard') ?>><a href="<?= $_url?>dashboard">Dashboard</a></li>
            <li <?= $checkActive('transaksi') ?>><a href="<?= $_url?>transaksi">Data Transaksi</a></li>
            <li <?= $checkActive('peminjam') ?>><a href="<?= $_url?>peminjam">Data Peminjam</a></li>
            <?php if ($_access == 'admin'): ?>
            <li <?= $checkActive('user') ?>><a href="<?= $_url?>user">Pengguna</a></li>
            <?php endif; ?>
            <li <?= $checkActive('rekap') ?>><a href="<?= $_url?>rekap">Rekap Data</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li>
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?= $_username ?> <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="<?= $_url ?>user/change-password">Ubah Password</a></li>
              <li><a href="<?= $_url ?>sign/out">Logout</a></li>
          </ul>
          </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li <?= $checkActive('dashboard') ?>><a href="<?= $_url?>dashboard">Dashboard</a></li>
            <li <?= $checkActive('transaksi') ?>><a href="<?= $_url?>transaksi">Data Transaksi</a></li>
            <li <?= $checkActive('peminjam') ?>><a href="<?= $_url?>peminjam">Data Peminjam</a></li>
            <?php if ($_access == 'admin'): ?>
            <li <?= $checkActive('user') ?>><a href="<?= $_url?>user">Pengguna</a></li>
            <?php endif; ?>
            <li <?= $checkActive('rekap') ?>><a href="<?= $_url?>rekap">Rekap Data</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <?= $_content ?>
        </div>
      </div>
    </div>
</body>
</html>