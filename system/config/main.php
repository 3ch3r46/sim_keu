<?php
date_default_timezone_set('Asia/Jakarta');
// starting session
session_start();

$GLOBALS['_format_kode'] = array(
	"year" => "Y",
	"number" => 4,
	"output" => "FT{year}{number}"
);

// set session
$GLOBALS['_access'] = isset($_SESSION['access']) ? $_SESSION['access'] : '';
$GLOBALS['_username'] = isset($_SESSION['username']) ? $_SESSION['username'] : '';
$GLOBALS['_name'] = isset($_SESSION['name']) ? $_SESSION['name'] : '';

// configuration conection with format 'host','username','password','dbname'
$GLOBALS['koneksi'] = mysqli_connect('localhost','anam','123qwe!@#','sistem_keuangan');

// configuration access rule who can access page
$GLOBALS['_rules'] = array(
	'dashboard/index' => array('admin','user'),
	
	// route sign
	'sign/in' => array(''),
	'sign/out' => array('admin', 'user'),

	'transaksi/add' => array('admin', 'user'),
	'transaksi/edit' => array('admin', 'user'),
	'transaksi/index' => array('admin', 'user'),
	'transaksi/view' => array('admin', 'user'),
	'transaksi/delete' => array('admin', 'user'),

	'peminjam/add' => array('admin', 'user'),
	'peminjam/edit' => array('admin', 'user'),
	'peminjam/index' => array('admin', 'user'),
	'peminjam/view' => array('admin', 'user'),
	'peminjam/delete' => array('admin', 'user'),

	'user/add' => array('admin'),
	'user/index' => array('admin'),
	'user/delete' => array('admin'),

	'user/change-password' => array('admin','user'),
);

$GLOBALS['_url'] = str_replace('/index.php', '/', $_SERVER['PHP_SELF']);
$GLOBALS['_route'] = explode($_url, explode("?", $_SERVER['REQUEST_URI'])[0], 2)[1];

$GLOBALS['_theme'] = 'main';