<?php
function formatRupiah($number)
{
    return "Rp " . number_format($number, 0, ',', '.');
}

function insertData($koneksi, $table, $data = []) {
    $sql = "insert into {$table} set ";

    $setter = [];
    foreach($data as $field=>$value) {
        $setter[] = $field.'="'.$value.'"';
    }
    $sql.= implode(',', $setter);

    $res = mysqli_query($koneksi, $sql);
    return $res;
}

function updateData($koneksi, $table, $data = [], $condition=null) {
    $sql = "update {$table} set ";

    $setter = [];
    foreach($data as $field=>$value) {
        $setter[] = $field.'="'.$value.'"';
    }
    $sql.= implode(',', $setter);

    if ($condition != null) {
        $sql.= " where {$condition}";
    }

    $res = mysqli_query($koneksi, $sql);
    return $res;
}

function deleteData($koneksi, $table, $condition=null) {
    $sql = "delete from {$table} ";

    if ($condition != null) {
        $sql.= " where {$condition}";
    }

    $res = mysqli_query($koneksi, $sql);
    return $res;
}

function fetchData($koneksi, $table, $condition = null, $option = null) {
    $sql = "select * from {$table}";
    if ($condition != null) {
        $sql.= " where {$condition}";
    }

    if ($option != null) {
        $sql.= " " . $option;
    }

    $res = mysqli_query($koneksi, $sql);
    $result = [];
    while($data = mysqli_fetch_array($res)) {
        $result[] = $data;
    }
    return $result;
}

function fetchDataBySql($koneksi, $sql) {
    $res = mysqli_query($koneksi, $sql);
    $result = [];
    while($data = mysqli_fetch_array($res)) {
        $result[] = $data;
    }
    return $result;
}

function alertMessage($type, $message) {
    return '<div class="alert alert-'.$type.'">'.$message.'</div>';
}

function redirectJs($url, $timeout) {
    echo "<script>
            setTimeout(function(){ window.location.href='{$url}'; }, {$timeout});
    </script>";
}

$GLOBALS['getKodeTransaksi'] = function() use($koneksi, $_format_kode){
    $query = mysqli_query($koneksi, "select kode from transaksi order by id desc limit 1");
    $fetch = mysqli_fetch_array($query);
    $kode = isset($fetch['kode']) ? $fetch['kode'] : null;

    $year = date($_format_kode['year']);
    $output = $_format_kode['output'];
    $yearPos = strpos($output, '{year}');
    $yearKode = substr($kode, $yearPos, strlen($year));
    $output=str_replace('{year}', $year, $output);
    $number = 1;
    if ($yearKode == $year) {
        $numberPos = strpos($output, '{number}');
        $number = substr($kode, $numberPos, $_format_kode['number']);
        $number = $number+1;
    }
    $output = str_replace('{number}', str_pad($number, $_format_kode['number'], "0", STR_PAD_LEFT),$output);
    return $output;
};