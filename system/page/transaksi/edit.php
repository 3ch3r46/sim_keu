<h3 class="page-header">
Edit Transaksi #<?= ucfirst($_id) ?>
</h3>

<?php
$field = fetchData($koneksi, "transaksi", "kode='{$_id}'")[0];

$_year = date("Y", strtotime($field['tanggal']));
$_month = date("m", strtotime($field['tanggal']));

extract($field);
if (isset($_POST['submit'])) {
	
	extract($_POST);

	$data = [
		'jenis' => $jenis,
		'kategori' => $kategori,
		'kode' => $kode,
		'jumlah' => $jumlah,
		'tanggal' => $tanggal,
		'keterangan' => $keterangan,
	];

	if (isset($id_peminjaman)) {
		$data['id_peminjaman'] = $id_peminjaman;
	}

	$errors = [];
	foreach($data as $key=>$value) {
		if ($value == '') {
			$errors[] = "<li>{$key} tidak boleh kosong.</li>";
		}
	}

	if (empty($errors))
		$query = updateData($koneksi, 'transaksi', $data, "kode='{$_id}'");

	if (empty($errors) && $query) {
		echo alertMessage('success', "Transaksi berhasil di ubah");
		redirectJs($_url.'transaksi/index/'.$_year.$_month, 2000);
	} else {
		echo alertMessage('danger', "Transaksi gagal di ubah" . (!empty($errors)?"<ul>".implode('', $errors)."</ul>":''));
	}
}
?>

<form method="post" class="form-horizontal">

	<?php
	if ($kategori == 'hutang'):
	$dataPeminjam = fetchData($koneksi, 'peminjaman');
	?>
		<div class="form-group">
			<label class="control-label col-sm-3">Peminjam</label>
			<div class="col-sm-6">
				<select id="id_peminjam" class="form-control select select-primary select2-offscreen" name="id_peminjaman">
					<option value="">Pilih Peminjam</option>
					<?php
					foreach($dataPeminjam as $pem):
					?>
						<option value="<?= $pem['id'] ?>" <?= isset($id_peminjaman) && $id_peminjaman == $pem['id'] ? 'selected':''?>><?= $pem['no_identitas']; ?> | <?= $pem['nama']; ?></option>
					<?php
					endforeach;
					?>
				</select>
			</div>
			<div class="col-sm-3">
				<a href="<?= $_url ?>peminjam/add" class="btn btn-primary">Tambah Peminjam</a>
			</div>
		</div>

		<script>
			$('#id_peminjam').select2();
		</script>
	<?php
	endif;
	?>

	<div class="form-group">
		<label class="control-label col-sm-3">Kode Transaksi</label>
		<div class="col-sm-6">
			<input class="form-control" type="text" name="kode" value="<?= $kode ?>">
		</div>
	</div>
	
	<div class="form-group">
		<label class="control-label col-sm-3">Tanggal</label>
		<div class="col-sm-6">
			<input class="form-control" type="date" name="tanggal" value="<?= isset($tanggal)?$tanggal:date("Y-m-d") ?>">
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-sm-3">Jumlah Nominal</label>
		<div class="col-sm-6">
			<input class="form-control" type="text" name="jumlah" value="<?= isset($jumlah)?$jumlah:'' ?>">
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-sm-3">Keterangan</label>
		<div class="col-sm-6">
			<textarea class="form-control" rows="5" name="keterangan"><?= isset($keterangan)?$keterangan:'' ?></textarea>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-3">
		<button type="submit" name="submit" class="btn btn-primary">Simpan</button>
		<a href="<?= $_url ?>transaksi/index/<?= $_year.$_month?>" class="btn btn-danger">Batal</a>
		</div>
	</div>

</div>

</form>