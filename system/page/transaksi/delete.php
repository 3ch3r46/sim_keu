<?php

if (isset($_params[1]) && $_params[1] == 'yes') {
$query = deleteData($koneksi, 'transaksi', "kode='{$_id}'");

	if ($query) {
		echo alertMessage('success', "Transaksi berhasil di hapus");
		redirectJs($_url.'transaksi', 2000);
	} else {
		echo alertMessage('danger', "Gagal menghapus transaksi");
	}
}
?>

<h3 class="page-header">Hapus Transaksi</h3>
<h4>Apakah anda yakin akan menghapus transaksi dengan Kode Transaksi #<?= $_id ?>?</h4>
<a href="<?= $_url ?>transaksi/delete/<?= $_id ?>/yes" class="btn btn-primary">Yes</a> <a href="<?= $_url ?>transaksi" class="btn btn-danger">No</a>