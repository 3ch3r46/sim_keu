<h3 class="page-header">
Transaksi
<span class="pull-right">
	<a href="<?= $_url ?>transaksi/add/pengeluaran" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span> Pengeluaran</a>
	<a href="<?= $_url ?>transaksi/add/pemasukan" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span> Pemasukan</a>
	<a href="<?= $_url ?>transaksi/add/peminjaman" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span> Peminjaman</a>
	<a href="<?= $_url ?>transaksi/add/pengembalian" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span> Pengembalian</a>
</span>
</h3>

<?php
	$_year = $_id != '' ? substr($_id, 0, 4) : date("Y");
	$_month = $_id != '' ? substr($_id, 4, 2) : date("m");
	$_bulankemarin = (($_month-1)==0) ? ($_year-1).'12' : $_year.($_month-1);
	$_bulandepan = (($_month+1)==13) ? ($_year+1).'1' : $_year.($_month+1);
	$tipe = array(
		'keluar-lain-lain' => 'Pengeluaran',
		'masuk-lain-lain' => 'Pemasukan',
		'keluar-hutang' => 'Peminjaman',
		'masuk-hutang' => 'Pengembalian',
	);

	$bydate = date("Y-m-d", strtotime($_year.'-'.$_month . '-31'));
	$kalku = fetchDataBySql($koneksi, "SELECT jenis,SUM(jumlah) as 'jumlah' FROM `transaksi` WHERE date(tanggal) < '{$bydate}' GROUP BY jenis");

	$data = fetchData($koneksi, 'transaksi', "year(tanggal)={$_year} and month(tanggal)={$_month}");
?>

<h4><?= date("F Y", strtotime($_year.'-'.$_month)) ?> 
<span class="pull-right">
<a class="btn btn-primary btn-sm" href="<?= $_url ?>transaksi/index/<?= $_bulankemarin ?>">Bulan Kemarin</a>
<a class="btn btn-primary btn-sm" href="<?= $_url ?>transaksi/index/<?= $_bulandepan ?>">Bulan Depan</a>
</span>
</h4>

<table class="table striped hovered border bordered">
	<thead>
		<tr>
			<th>Kode Transaksi</th>
			<th>Tanggal</th>
			<th>Tipe</th>
			<th>Nominal</th>
			<th></th>
		</tr>
	</thead>
	<tbody>

	<?php
		if (!empty($data)):
			foreach($data as $field):
	?>
		<tr>
			<td><?= $field['kode'] ?></td>
			<td><?= $field['tanggal'] ?></td>
			<td><?= $tipe[$field['jenis'].'-'.$field['kategori']] ?></td>
			<td><?= formatRupiah($field['jumlah']) ?></td>
			<td>
				<a class="btn btn-primary btn-xs" href="<?= $_url ?>transaksi/view/<?= $field['kode'] ?>"><span class="mif-zoom-in"></span> View</a>
				<a class="btn btn-warning btn-xs" href="<?= $_url ?>transaksi/edit/<?= $field['kode'] ?>"><span class="mif-pencil"></span> Edit</a>
				<a class="btn btn-danger btn-xs" href="<?= $_url ?>transaksi/delete/<?= $field['kode'] ?>"><span class="mif-cross"></span> Delete</a>
			</td>
		</tr>
	<?php
			endforeach;
		else:
	?>
		<tr>
			<td colspan="6">
			Data tidak ditemukan
			</td>
		</tr>
	<?php
		endif;
	?>
		
	</tbody>
</table>

<a class="btn btn-primary" href="<?= $_url ?>transaksi/index/<?= $_bulankemarin ?>">Bulan Kemarin</a>
<a class="btn btn-primary pull-right" href="<?= $_url ?>transaksi/index/<?= $_bulandepan ?>">Bulan Depan</a>

<?php
$balance = [
	'masuk' => 0,
	'keluar' => 0
];
foreach($kalku as $fld) {
	$balance[$fld['jenis']] = $fld['jumlah'];
}
?>
<div class="panel panel-primary" style="margin-top:15px">
  <div class="panel-heading">Keuangan sampai bulan <?= date("F Y", strtotime($_year.'-'.$_month)) ?></div>
<table class="table">
	<tr>
	<th>Uang Masuk</th>
	<td><?= formatRupiah($balance['masuk']) ?></td>
	</tr>
	<tr>
	<th>Uang Keluar</th>
	<td><?= formatRupiah($balance['keluar']) ?></td>
	</tr>
	<tr>
	<th>Balance</th>
	<td><?= formatRupiah($balance['masuk']-$balance['keluar']) ?></td>
	</tr>
</table>
</div>