<style type="text/css">
	.input-control {
		border: 1px solid #d9d9d9;
		padding: 10px;
	}
</style>

<?php
$tipe = array(
		'keluar-lain-lain' => 'Pengeluaran',
		'masuk-lain-lain' => 'Pemasukan',
		'keluar-hutang' => 'Peminjaman',
		'masuk-hutang' => 'Pengembalian',
	);
$field = fetchData($koneksi, "transaksi", "kode='{$_id}'")[0];
$_year = date("Y", strtotime($field['tanggal']));
$_month = date("m", strtotime($field['tanggal']));
?>
<h3 class="page-header">
Transaksi #<?= $_id ?>
</h3>

<a href="<?= $_url ?>transaksi/index/<?= $_year.$_month?>" class="btn btn-primary" style="margin-bottom: 10px">Kembali</a>

<table class="table">
	<tr>
		<th>Kode Transaksi</th>
		<td><?= $field['kode'] ?></td>
	</tr>
	<tr>
		<th>Tipe</th>
		<td><?= $tipe[$field['jenis'].'-'.$field['kategori']] ?></td>
	</tr>
	<tr>
		<th>Tanggal Transaksi</th>
		<td><?= $field['tanggal'] ?></td>
	</tr>
	<tr>
		<th>Nominal</th>
		<td><?= formatRupiah($field['jumlah']) ?></td>
	</tr>
	<tr>
		<th>Keterangan</th>
		<td><?= $field['keterangan'] ?></td>
	</tr>
</table>