<h3 class="page-header">Sign Out?</h3>
<?php

if ($_id == 'yes') {

		session_destroy();

		echo "
		<div class='alert alert-success'>Anda telah berhasil logout</div>
		<script>
		setTimeout(function(){ window.location.href='{$_url}'; }, 2000);
		</script>";
}
?>

<h3>Apakah anda yakin akan Logout?</h3>
<a href="<?= $_url ?>sign/out/yes" class="btn btn-primary">Yes</a> <a href="<?= $_url ?>" class="btn btn-danger">No</a>