<?php
$GLOBALS['_theme'] = 'blank';
?>

    <style>
        .login-form {
            width: 45rem;
            position: fixed;
            top: 50%;
            margin-top: -15.375rem;
            left: 50%;
            margin-left: -13.5rem;
            background-color: #edeff1;
            opacity: 0;
            -webkit-transform: scale(.8);
            transform: scale(.8);
        }
        .login-icon {
            left: -170px;
            top: 0;
            width: 150px;
        }
        body {
            background: #48c9b0;
        }
    </style>

    <script>

        $(function(){
            var form = $(".login-form");

            form.css({
                opacity: 1,
                "-webkit-transform": "scale(1)",
                "transform": "scale(1)",
                "-webkit-transition": ".5s",
                "transition": ".5s"
            });
        });
    </script>
    
    <?php
    $username = '';
    $password = '';
    $summary = "";
    if (isset($_POST['submit'])) {
        extract($_POST);

        $sql = "SELECT * FROM pengguna WHERE username='{$username}' AND password=md5('{$password}')";
        
        $query = mysqli_query($koneksi, $sql);

        if (mysqli_num_rows($query) == 1) {
            $field = mysqli_fetch_array($query);
            $_SESSION['access'] = $field['level'];
            $_SESSION['username'] = $field['username'];
            $summary = "
            <div class=\"alert alert-success\" role=\"alert\">Login Success!</div>
            <script>
            setTimeout(function(){ window.location.href='{$_url}'; }, 2000);
            </script>";
        } else {
            $summary = "
            <div class=\"alert alert-danger\" role=\"alert\">Login Gagal!<br>Periksa Username dan Password anda!!</div>
            ";
        }
    }
    ?>
        <form class="login-form" method="post">
            <?= $summary ?>
          <div class="login-icon">
            <img src="<?= $_url ?>assets/img/logo.png" alt="Selamat Datang Di SIM Keuangan">
            <h4>Selamat Datang Di <small>SIM Keuangan</small></h4>
          </div>

          <div class="login-form-div">
            <div class="form-group">
              <input type="text" class="form-control login-field" name="username" id="user_login" value="<?= $username ?>" placeholder="Username">
              <label class="login-field-icon fui-user" for="login-name"></label>
            </div>

            <div class="form-group">
              <input type="password" name="password" id="user_password" value="<?= $password ?>" class="form-control login-field" placeholder="Password">
              <label class="login-field-icon fui-lock" for="login-pass"></label>
            </div>

            <button type="submit" name="submit" class="btn btn-primary btn-lg btn-block" href="#">Log in</button>
          </div>
        </form>