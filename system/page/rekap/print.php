<?php
$GLOBALS['_theme'] = 'blank';

extract($_POST);

$uang = array(
    'keluar' => 0,
    'masuk' => 0
);

$bulans = array(
    '1' => 'Januari',
    '2' => 'Februari',
    '3' => 'Maret',
    '4' => 'April',
    '5' => 'Mei',
    '6' => 'Juni',
    '7' => 'Juli',
    '8' => 'Agustus',
    '9' => 'September',
    '10' => 'Oktober',
    '11' => 'November',
    '12' => 'Desember'
);
if (!isset($tahun))
    header('location:'.$_url.'rekap');
$_bulankemarin = (!isset($bulan) || ($bulan-1)==0) ? ($tahun-1).'-12' : $tahun.'-'.($bulan-1);
$bydate = date("Y-m-d", strtotime($_bulankemarin . '-31'));
$kalku = fetchDataBySql($koneksi, "SELECT jenis,SUM(jumlah) as 'jumlah' FROM `transaksi` WHERE date(tanggal) < '{$bydate}' GROUP BY jenis");
$data = fetchDataBySql($koneksi, "SELECT tr.*, pm.no_identitas from transaksi tr left join peminjaman pm on (tr.id_peminjaman=pm.id) where year(tanggal)='{$tahun}'" . (isset($bulan)?" and month(tanggal)='{$bulan}'":'') . (isset($jenis) && $jenis!=''?" and jenis='{$jenis}'":''));

foreach($kalku as $fld) {
    $uang[$fld['jenis']] = $fld['jumlah'];
}
$saldo = $uang['masuk']-$uang['keluar'];
?>

<style>
td,th {
    padding: 5px;
    font-size: 11px;
}
</style>
<script>
window.print();
setTimeout(function(){ window.close();},100);
</script>

<div class="header" style="text-align:center;height:100px;border-bottom:2px solid #000;margin-bottom:10px;">
<img src="<?= $_url ?>assets/img/logo.png" width="80"/ style="position:absolute;left:3px;top:3px">
<div style="width:100%;font-size:18px;font-weight:bold">UNIVERSITAS MUHAMMADIYAH JEMBER</div>
<div style="width:100%;font-size:20px;font-weight:bold">FAKULTAS TEKNIK</div>
<div style="width:100%;font-size:22px;font-weight:bold">PROGRAM STUDI TEKNIK INFORMATIKA</div>
</div>

<div style="width:100%;font-size:18px;font-weight:bold;text-align:center"><?= isset($jenis) && $jenis!=''?ucfirst($tipe[$jenis]): 'Transaksi' ?> pada <?= isset($bulan) ? $bulans[$bulan].' ':'' ?><?= $tahun ?></div>

<table border="1" width="100%">
    <thead>
        <tr>
            <th>No</th>
            <th>Kode Transaksi</th>
            <th>ID Peminjam</th>
            <th>Tanggal</th>
            <th>Debit</th>
            <th>Kredit</th>
            <th>Saldo</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no=1;
        $debit = 0;
        $kredit = 0;
        if ($saldo != 0):
        ?>
        <tr>
            <td><?= $no ?></td>
            <td>MUTASI</td>
            <td></td>
            <td></td>
            <td><?php
            if ($saldo > 0) {
                $debit += $saldo;
                echo formatRupiah($saldo);
            } ?></td>
            <td><?php
            if ($saldo < 0) {
                $kredit += $saldo;
                echo formatRupiah($saldo);
            } ?></td>
            <td><?php
                echo formatRupiah($saldo);
            ?></td>
        </tr>
        <?php
        $no++;
        endif;
        foreach($data as $field):
        ?>
        <tr>
            <td><?= $no ?></td>
            <td><?= $field['kode'] ?></td>
            <td><?= $field['no_identitas'] != '' ? $field['no_identitas'] : '-' ?></td>
            <td><?= date("d/m/Y", strtotime($field['tanggal'])) ?></td>
            <td><?php
            if ($field['jenis'] == "masuk") {
                $saldo += $field['jumlah'];
                $debit += $field['jumlah'];
                echo formatRupiah($field['jumlah']);
            } ?></td>
            <td><?php
            if ($field['jenis'] == "keluar") {
                $saldo -= $field['jumlah'];
                $kredit += $field['jumlah'];
                echo formatRupiah($field['jumlah']);
            } ?></td>
            <td><?php
                echo formatRupiah($saldo);
            ?></td>
        </tr>
        <?php
        $no++;
        endforeach;
        ?>
        <tr>
            <td colspan="4">Total</td>
            <td><?= formatRupiah($debit) ?></td>
            <td><?= formatRupiah($kredit) ?></td>
            <td><?= formatRupiah($saldo) ?></td>
        </tr>
    </tbody>
</table>