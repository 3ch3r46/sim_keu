<h3 class="page-header">Rekap Data</h3>

<div class="row">

<div class="col-sm-6">
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Per-Bulan</h3>
  </div>
  <div class="panel-body">
<form method="post" target="_blank" class="form-horizontal" action="<?= $_url ?>rekap/print">

	<div class="form-group">
		<label class="control-label col-sm-4">Tahun</label>
		<div class="col-sm-6">
			<select class="form-control" name="tahun">
                <?php
                $tahun = fetchDataBySql($koneksi, "select year(tanggal) as tahun from transaksi group by year(tanggal) order by year(tanggal)");
                foreach($tahun as $label) {
                    echo "<option value='{$label['tahun']}'>{$label['tahun']}</option>";
                }?>
            </select>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-sm-4">Bulan</label>
		<div class="col-sm-6">
            <select class="form-control" name="bulan">
                <?php
                $bulan = array(
                    '1' => 'Januari',
                    '2' => 'Februari',
                    '3' => 'Maret',
                    '4' => 'April',
                    '5' => 'Mei',
                    '6' => 'Juni',
                    '7' => 'Juli',
                    '8' => 'Agustus',
                    '9' => 'September',
                    '10' => 'Oktober',
                    '11' => 'November',
                    '12' => 'Desember'
                );
                foreach($bulan as $val=>$label) {
                    echo "<option value='{$val}'>{$label}</option>";
                }?>
            </select>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4">
		<button type="submit" name="submit" class="btn btn-primary">Submit</button>
		<a href="<?= $_url ?>transaksi" class="btn btn-danger">Batal</a>
		</div>
	</div>

</form>
</div>
</div>
</div>

<div class="col-sm-6">
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Per-Tahun</h3>
  </div>
  <div class="panel-body">
<form method="post" class="form-horizontal" target="_blank" action="<?= $_url ?>rekap/print">

	<div class="form-group">
		<label class="control-label col-sm-4">Tahun</label>
		<div class="col-sm-6">
			<select class="form-control" name="tahun">
                <?php
                $tahun = fetchDataBySql($koneksi, "select year(tanggal) as tahun from transaksi group by year(tanggal) order by year(tanggal)");
                foreach($tahun as $label) {
                    echo "<option value='{$label['tahun']}'>{$label['tahun']}</option>";
                }?>
            </select>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4">
		<button type="submit" name="submit" class="btn btn-primary">Submit</button>
		<a href="<?= $_url ?>transaksi" class="btn btn-danger">Batal</a>
		</div>
	</div>

</form>
</div>
</div>
</div>

</div>