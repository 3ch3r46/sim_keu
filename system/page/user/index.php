<h3 class="page-header">
Pengguna
<span class="pull-right">
	<a href="<?= $_url ?>user/add" class="btn btn-primary">Tambah Pengguna</a>
</span>
</h3>

<?php
	$data = fetchData($koneksi, 'pengguna');
?>

<table class="table striped hovered border bordered">
	<thead>
		<tr>
			<th>Username</th>
			<th>Level</th>
			<th></th>
		</tr>
	</thead>
	<tbody>

	<?php
		if (!empty($data)):
			foreach($data as $field):
	?>
		<tr>
			<td><?= $field['username'] ?></td>
			<td><?= $field['level'] ?></td>
			<td>
				<a class="btn btn-warning btn-xs" href="<?= $_url ?>user/edit/<?= $field['id'] ?>/<?= urlencode($field['username']) ?>"><span class="mif-pencil"></span> Edit</a>
				<a class="btn btn-danger btn-xs" href="<?= $_url ?>user/delete/<?= $field['id'] ?>/<?= urlencode($field['username']) ?>"><span class="mif-cross"></span> Delete</a>
			</td>
		</tr>
	<?php
			endforeach;
		else:
	?>
		<tr>
			<td colspan="3">
			Data tidak ditemukan
			</td>
		</tr>
	<?php
		endif;
	?>
		
	</tbody>
</table>