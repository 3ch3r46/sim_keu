<?php


?>
<h3 class="page-header">Ubah Password</h3>

<?php
$password = '';
$password_repeat = '';
if (isset($_POST['submit'])) {

	extract($_POST);

	$sql = "UPDATE user SET password=md5('{$password}') WHERE username='{$_username}'";

	if ($password == $password_repeat && mysqli_query($koneksi, $sql)) {
		echo alertMessage('success', "Password berhasil diubah");
	} else {
		echo alertMessage('danger', "Password gagal diubah");
	}
}
?>

<form method="post" class="form-horizontal">

<div class="form-group">
		<label class="control-label col-sm-3">Password Baru</label>
		<div class="col-sm-6">
			<input type="password" name="password" class="form-control" value="<?= $password ?>">
		</div>
</div>
<div class="form-group">
		<label class="control-label col-sm-3">Ulangi Password Baru</label>
		<div class="col-sm-6">
			<input type="password" name="password_repeat" class="form-control" value="<?= $password_repeat ?>">
		</div>
</div>

<div class="form-group">
	<div class="col-sm-6 col-sm-offset-3">
		<button type="submit" name="submit" class="btn btn-primary">SUBMIT</button>
	</div>
</div>

</form>