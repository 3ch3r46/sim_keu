<?php
$field = fetchData($koneksi, 'pengguna', "id='{$_id}'")[0];
extract($field);
?>
<h3 class="page-header">
Edit Pengguna
</h3>

<?php

if (isset($_POST['submit'])) {

	extract($_POST);

	$setdb = array("username='{$username}'","level='{$level}'");

	if ($password != null)
		$setdb[] = "password=md5('{$password}')";

	$setdb = implode(',', $setdb);

	$sql = "UPDATE pengguna SET {$setdb} WHERE id='{$_id}'";
	$query = mysqli_query($koneksi, $sql);

	if ($query) {
		echo alertMessage('success', "Data User berhasil diubah");
		redirectJs($_url.'user', 2000);
	} else {
		echo alertMessage('danger', "Data User gagal diubah");
	}
}
?>

<form method="post" class="form-horizontal">

<div class="form-group">
	<label class="control-label col-sm-3">User ID</label>
	<div class="col-sm-6">
		<input class="form-control" type="text" name="username" value="<?= isset($username)?$username:'' ?>">
	</div>
</div>

<div class="form-group">
	<label class="control-label col-sm-3">Password</label>
	<div class="col-sm-6">
		<input class="form-control" type="password" name="password">
	</div>
</div>

<div class="form-group">
	<label class="control-label col-sm-3">Level</label>
	<div class="col-sm-6">
		<div class="radio">
		<label>
			<input type="radio" name="level" value="admin" <?= isset($level) && $level=='admin'?'checked':'' ?>>
			Admin
		</label>
		</div>
		<div class="radio">
		<label>
			<input type="radio" name="level" value="user" <?= isset($level) && $level=='user'?'checked':'' ?>>
			User
		</label>
		</div>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-6 col-sm-offset-3">
	<button type="submit" name="submit" class="btn btn-primary">Simpan</button>
	<a href="<?= $_url ?>user" class="btn btn-danger">Batal</a>
	</div>
</div>

</div>

</form>