<?php

if (isset($_params[2]) && $_params[2] == 'yes') {
$query = deleteData($koneksi, 'pengguna', "id='{$_id}'");

	if ($query) {
		echo alertMessage('success', "Pengguna berhasil dihapus");
		redirectJs($_url.'user', 2000);
	} else {
		echo alertMessage('danger', "Pengguna gagal dihapus");
	}
}
?>

<h3 class="page-header">Hapus Pengguna</h3>
<h5>Apakah anda yakin akan menghapus pengguna dengan User ID "<?= urldecode($_params[1]) ?>"?</h5>
<a href="<?= $_url ?>user/delete/<?= $_id ?>/<?= $_params[1] ?>/yes" class="btn btn-primary">Yes</a> <a href="<?= $_url ?>user" class="btn btn-danger">No</a>