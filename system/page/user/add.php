<h3 class="page-header">
Tambah Pengguna
</h3>

<?php
if (isset($_POST['submit'])) {

	extract($_POST);

	$sql = "INSERT INTO pengguna(username, password, level, active) values('{$username}', md5('{$password}'), '{$level}', 1)";
	$query = mysqli_query($koneksi, $sql);

	if ($query) {
		echo alertMessage('success', "Data User berhasil ditambah");
		redirectJs($_url.'user', 2000);
	} else {
		echo alertMessage('danger', "Data User gagal ditambah");
	}
}
?>

<form method="post" class="form-horizontal">

<div class="form-group">
	<label class="control-label col-sm-3">User ID</label>
	<div class="col-sm-6">
		<input class="form-control" type="text" name="username" value="<?= isset($username)?$username:'' ?>">
	</div>
</div>

<div class="form-group">
	<label class="control-label col-sm-3">Password</label>
	<div class="col-sm-6">
		<input class="form-control" type="password" name="password" value="<?= isset($password)?$password:'' ?>">
	</div>
</div>

<div class="form-group">
	<label class="control-label col-sm-3">Level</label>
	<div class="col-sm-6">
		<div class="radio">
		<label>
			<input type="radio" name="level" value="admin" <?= isset($level) && $level=='admin'?'checked':'' ?>>
			Admin
		</label>
		</div>
		<div class="radio">
		<label>
			<input type="radio" name="level" value="user" <?= isset($level) && $level=='user'?'checked':'' ?>>
			User
		</label>
		</div>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-6 col-sm-offset-3">
	<button type="submit" name="submit" class="btn btn-primary">Simpan</button>
	<a href="<?= $_url ?>user" class="btn btn-danger">Batal</a>
	</div>
</div>

</div>

</form>