<?php
$year = date("Y");
$month = date("m");
$bydate = date("Y-m-d", strtotime($year.'-'.$month . '-31'));
$kalku = fetchDataBySql($koneksi, "SELECT jenis,SUM(jumlah) as 'jumlah' FROM `transaksi` WHERE year(tanggal)='{$year}' and month(tanggal)='{$month}' GROUP BY jenis");
$kalkuB = fetchDataBySql($koneksi, "SELECT jenis,SUM(jumlah) as 'jumlah' FROM `transaksi` WHERE date(tanggal) <= '{$bydate}' GROUP BY jenis");
$totalTransaksi = fetchDataBySql($koneksi, "SELECT count(*) as 'jumlah' FROM `transaksi` WHERE year(tanggal)='{$year}' and month(tanggal)='{$month}'")[0];
$balance = [
	'masuk' => 0,
	'keluar' => 0
];
foreach($kalku as $fld) {
	$balance[$fld['jenis']] = $fld['jumlah'];
}
$balanced = [
	'masuk' => 0,
	'keluar' => 0
];
foreach($kalkuB as $fld) {
	$balanced[$fld['jenis']] = $fld['jumlah'];
}
$tipe = array(
		'keluar-lain-lain' => 'Pengeluaran',
		'masuk-lain-lain' => 'Pemasukan',
		'keluar-hutang' => 'Peminjaman',
		'masuk-hutang' => 'Pengembalian',
	);
$transaksiTerakhir = fetchData($koneksi, 'transaksi', null, "order by id desc limit 5");
?>

<h3 class="page-header">Dashboard</h3>
<div class="row placeholders">
    <div class="col-xs-6 col-sm-3 placeholder">
        <div style="word-wrap:break-word;width:100%;padding: 30px 10px;border-radius: 20px;background:#1ABC9C;color:#fff"><?= formatRupiah($balance['masuk']) ?></div>
        <h5>Total Uang Masuk</h5>
        <span class="text-muted">Bulan Ini</span>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <div style="word-wrap:break-word;width:100%;padding: 30px 10px;border-radius: 20px;background:#1ABC9C;color:#fff"><?= formatRupiah($balance['keluar']) ?></div>
        <h5>Total Uang Keluar</h5>
        <span class="text-muted">Bulan Ini</span>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <div style="word-wrap:break-word;width:100%;padding: 30px 10px;border-radius: 20px;background:#1ABC9C;color:#fff"><?= formatRupiah($balanced['masuk']-$balanced['keluar']) ?></div>
        <h5>Total Balance</h5>
        <span class="text-muted">Sampai bulan ini</span>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <div style="word-wrap:break-word;width:100%;padding: 30px 10px;border-radius: 20px;background:#1ABC9C;color:#fff"><?= $totalTransaksi['jumlah'] ?></div>
        <h5>Jumlah Transaksi</h5>
        <span class="text-muted">Bulan Ini</span>
    </div>
</div>

<div class="panel panel-primary" style="margin-top:15px">
	<div class="panel-heading">
	5 Transaksi Terakhir
    <span class="pull-right">
        <a href="<?= $_url ?>transaksi/add/pengeluaran" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span> Pengeluaran</a>
        <a href="<?= $_url ?>transaksi/add/pemasukan" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span> Pemasukan</a>
        <a href="<?= $_url ?>transaksi/add/peminjaman" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span> Peminjaman</a>
        <a href="<?= $_url ?>transaksi/add/pengembalian" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span> Pengembalian</a>
    </span>
	</div>

<table class="table striped hovered border bordered">
	<thead>
		<tr>
			<th>Kode Transaksi</th>
			<th>Tanggal</th>
			<th>Tipe</th>
			<th>Nominal</th>
		</tr>
	</thead>
	<tbody>

	<?php
		if (!empty($transaksiTerakhir)):
			foreach($transaksiTerakhir as $field):
	?>
		<tr>
			<td><?= $field['kode'] ?></td>
			<td><?= $field['tanggal'] ?></td>
			<td><?= $tipe[$field['jenis'].'-'.$field['kategori']] ?></td>
			<td><?= formatRupiah($field['jumlah']) ?></td>
		</tr>
	<?php
			endforeach;
		else:
	?>
		<tr>
			<td colspan="4">
			Data tidak ditemukan
			</td>
		</tr>
	<?php
		endif;
	?>
		
	</tbody>
</table>
</div>