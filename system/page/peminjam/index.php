<h3 class="page-header">
Data Peminjam
<span class="pull-right">
	<a href="<?= $_url ?>peminjam/add" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Tambah Data</a>
</span>
</h3>

<?php
	$data = fetchData($koneksi, 'peminjaman');
?>


<table class="table striped hovered border bordered">
	<thead>
		<tr>
			<th>No Identitas</th>
			<th>Nama Peminjam</th>
			<th></th>
		</tr>
	</thead>
	<tbody>

	<?php
		if (!empty($data)):
			foreach($data as $field):
	?>
		<tr>
			<td><?= $field['no_identitas'] ?></td>
			<td><?= $field['nama'] ?></td>
			<td>
				<a class="btn btn-primary btn-xs" href="<?= $_url ?>peminjam/view/<?= $field['id'] ?>"><span class="mif-zoom-in"></span> View</a>
				<a class="btn btn-warning btn-xs" href="<?= $_url ?>peminjam/edit/<?= $field['id'] ?>"><span class="mif-pencil"></span> Edit</a>
				<a class="btn btn-danger btn-xs" href="<?= $_url ?>peminjam/delete/<?= $field['id'] ?>"><span class="mif-cross"></span> Delete</a>
			</td>
		</tr>
	<?php
			endforeach;
		else:
	?>
		<tr>
			<td colspan="3">
			Data tidak ditemukan
			</td>
		</tr>
	<?php
		endif;
	?>
		
	</tbody>
</table>