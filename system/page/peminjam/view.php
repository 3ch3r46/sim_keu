<style type="text/css">
	.input-control {
		border: 1px solid #d9d9d9;
		padding: 10px;
	}
</style>

<?php
$field = fetchData($koneksi, "peminjaman", "id='{$_id}'")[0];
?>
<h3 class="page-header">
Data Peminjam
<a href="<?= $_url ?>peminjam" class="btn btn-primary pull-right" style="margin-bottom: 10px">Kembali</a>
</h3>
<div class="panel panel-primary" style="margin-top:15px">
	<div class="panel-heading">Detail</div>
<table class="table">
	<tr>
		<th>No Identitas</th>
		<td><?= $field['no_identitas'] ?></td>
	</tr>
	<tr>
		<th>Nama Peminjam</th>
		<td><?= $field['nama'] ?></td>
	</tr>
</table>
</div>

<?php
	$_year = isset($_params[1]) ? substr($_params[1], 0, 4) : date("Y");
	$_month = isset($_params[1]) ? substr($_params[1], 4, 2) : date("m");
	$_bulankemarin = (($_month-1)==0) ? ($_year-1).'12' : $_year.($_month-1);
	$_bulandepan = (($_month+1)==13) ? ($_year+1).'1' : $_year.($_month+1);
	$tipe = array(
		'keluar' => 'Peminjaman',
		'masuk' => 'Pengembalian',
	);

	$bydate = date("Y-m-d", strtotime($_year.'-'.$_month . '-31'));
	$kalku = fetchDataBySql($koneksi, "SELECT jenis,SUM(jumlah) as 'jumlah' FROM `transaksi` WHERE kategori='hutang' and id_peminjaman='{$field['id']}' and date(tanggal) < '{$bydate}' GROUP BY jenis");

	$data = fetchData($koneksi, 'transaksi', "kategori='hutang' and id_peminjaman='{$field['id']}' and year(tanggal)={$_year} and month(tanggal)={$_month}");
?>

<div class="panel panel-primary" style="margin-top:15px">
	<div class="panel-heading">
	Transaksi: <?= date("F Y", strtotime($_year.'-'.$_month)) ?>
	<span class="pull-right">
	<a class="btn btn-primary btn-xs" href="<?= $_url ?>peminjam/view/<?= $_id ?>/<?= $_bulankemarin ?>">Bulan Kemarin</a>
	<a class="btn btn-primary btn-xs" href="<?= $_url ?>peminjam/view/<?= $_id ?>/<?= $_bulandepan ?>">Bulan Depan</a>
	</span>
	</div>

<table class="table striped hovered border bordered">
	<thead>
		<tr>
			<th>Kode Transaksi</th>
			<th>Tanggal</th>
			<th>Tipe</th>
			<th>Nominal</th>
			<th></th>
		</tr>
	</thead>
	<tbody>

	<?php
		if (!empty($data)):
			foreach($data as $field):
	?>
		<tr>
			<td><?= $field['kode'] ?></td>
			<td><?= $field['tanggal'] ?></td>
			<td><?= $tipe[$field['jenis']] ?></td>
			<td><?= formatRupiah($field['jumlah']) ?></td>
			<td>
				<a class="btn btn-primary btn-xs" href="<?= $_url ?>transaksi/view/<?= $field['kode'] ?>"><span class="mif-zoom-in"></span> View</a>
				<a class="btn btn-warning btn-xs" href="<?= $_url ?>transaksi/edit/<?= $field['kode'] ?>"><span class="mif-pencil"></span> Edit</a>
				<a class="btn btn-danger btn-xs" href="<?= $_url ?>transaksi/delete/<?= $field['kode'] ?>"><span class="mif-cross"></span> Delete</a>
			</td>
		</tr>
	<?php
			endforeach;
		else:
	?>
		<tr>
			<td colspan="6">
			Data tidak ditemukan
			</td>
		</tr>
	<?php
		endif;
	?>
		
	</tbody>
</table>
</div>

<?php
$balance = [
	'masuk' => 0,
	'keluar' => 0
];
foreach($kalku as $fld) {
	$balance[$fld['jenis']] = $fld['jumlah'];
}
?>
<div class="panel panel-primary" style="margin-top:15px">
	<div class="panel-heading">Keuangan sampai bulan <?= date("F Y", strtotime($_year.'-'.$_month)) ?></div>
<table class="table">
	<tr>
	<th>Uang Pengembalian</th>
	<td><?= formatRupiah($balance['masuk']) ?></td>
	</tr>
	<tr>
	<th>Uang Peminjaman</th>
	<td><?= formatRupiah($balance['keluar']) ?></td>
	</tr>
	<tr>
	<th>Jumlah Hutang</th>
	<td><?= formatRupiah($balance['keluar']-$balance['masuk']) ?></td>
	</tr>
</table>
</div>