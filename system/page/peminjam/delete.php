<?php

if (isset($_params[1]) && $_params[1] == 'yes') {
$query = deleteData($koneksi, 'peminjaman', "id='{$_id}'");

	if ($query) {
		echo alertMessage('success', "Transaksi berhasil di hapus");
		redirectJs($_url.'peminjam', 2000);
	} else {
		echo alertMessage('danger', "Gagal menghapus transaksi");
	}
}
?>

<h3 class="page-header">Hapus Data Peminjam</h3>
<h4>Apakah anda yakin akan menghapus data peminjam?</h4>
<a href="<?= $_url ?>peminjam/delete/<?= $_id ?>/yes" class="btn btn-primary">Yes</a> <a href="<?= $_url ?>peminjam" class="btn btn-danger">No</a>