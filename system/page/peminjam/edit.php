<h3 class="page-header">
Edit Peminjam
</h3>

<?php
$field = fetchData($koneksi, "peminjaman", "id='{$_id}'")[0];

extract($field);
if (isset($_POST['submit'])) {
	
	extract($_POST);
	
	$data = [
		'nama' => $nama,
		'no_identitas' => $no_identitas,
	];

	$errors = [];
	foreach($data as $key=>$value) {
		if ($value == '') {
			$errors[] = "<li>{$key} tidak boleh kosong.</li>";
		}
	}

	if (empty($errors))
		$query = updateData($koneksi, 'peminjaman', $data, "id='{$_id}'");;

	if (empty($errors) && $query) {
		echo alertMessage('success', "Data Peminjam berhasil di tambahkan");
		redirectJs($_url.'peminjam', 2000);
	} else {
		echo alertMessage('danger', "Data Peminjam gagal di tambahkan" . (!empty($errors)?"<ul>".implode('', $errors)."</ul>":''));
	}
}
?>

<form method="post" class="form-horizontal">

	<div class="form-group">
		<label class="control-label col-sm-3">No IDENTITAS</label>
		<div class="col-sm-6">
			<input class="form-control" type="text" name="no_identitas" value="<?= isset($no_identitas)?$no_identitas:'' ?>">
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-sm-3">Nama Peminjam</label>
		<div class="col-sm-6">
			<input class="form-control" type="text" name="nama" value="<?= isset($nama)?$nama:'' ?>">
		</div>
	</div>	

	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-3">
		<button type="submit" name="submit" class="btn btn-primary">Simpan</button>
		<a href="<?= $_url ?>peminjam" class="btn btn-danger">Batal</a>
		</div>
	</div>

</div>

</form>